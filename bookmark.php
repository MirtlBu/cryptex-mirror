<div class="bookmark">
    <div class="logo">
        <div class="square-bunch"></div>
        <div class="logo__text">
            <span class="logo__text--black">Cryptex<span class="logo__text--gray">24</span></span>

        </div>
        <div class="square-line-short"></div>
    </div>
    <div class="bookmark__nav">
        <div href="#" class="bookmark__navlinks bookmark__navlinks--active" data-nav="reserve">Reserve</div>
        <div href="#" class="bookmark__navlinks bookmark__navlinks--midl" data-nav="best">Best deals</div>
        <div href="#" class="bookmark__navlinks" data-nav="exchange">Exchange</div>
    </div>
    <div class="bookmark__tab">

        <div class="bookmark__item bookmark__item--reserve bookmark__item--active">
            <ul>
                <li class="bookmark__row bookmark__row--header">
                    <div class="bookmark__col align-left"><span>Name</span></div>
                    <div class="bookmark__col align-right"><span>Reserve</span></div>
                </li>
                <li class="bookmark__row bookmark__row--body">
                    <div class="bookmark__col align-left"><span>Webmoney</span></div>
                    <div class="bookmark__col align-right"><span>14000 usd</span></div>
                </li>
                <li class="bookmark__row bookmark__row--body">
                    <div class="bookmark__col align-left"><span>Perfectmoney</span></div>
                    <div class="bookmark__col align-right"><span>45000 usd</span></div>
                </li>
                <li class="bookmark__row bookmark__row--body">
                    <div class="bookmark__col align-left"><span>Bitcoin</span></div>
                    <div class="bookmark__col align-right"><span>500 usd</span></div>
                </li>
            </ul>
        </div>

        <div class="bookmark__item bookmark__item--best">
            <ul>
                <li class="bookmark__row bookmark__row--header">
                    <div class="bookmark__col align-left"><span>From</span></div>
                    <div class="bookmark__col align-left"><span>To</span></div>
                    <div class="bookmark__col align-right"><span>Percent</span></div>
                </li>
                <li class="bookmark__row bookmark__row--body">
                    <div class="bookmark__col align-left"><span>Webmoney</span></div>
                    <div class="bookmark__col align-left"><span>Perfectmoney</span></div>
                    <div class="bookmark__col align-right"><span>5%</span></div>
                </li>
                <li class="bookmark__row bookmark__row--body">
                    <div class="bookmark__col align-left"><span>Perfectmoney</span></div>
                    <div class="bookmark__col align-left"><span>Qiwi</span></div>
                    <div class="bookmark__col align-right"><span>3%</span></div>
                </li>
                <li class="bookmark__row bookmark__row--body">
                    <div class="bookmark__col align-left"><span>Bitcoin</span></div>
                    <div class="bookmark__col align-left"><span>Sberbank</span></div>
                    <div class="bookmark__col align-right"><span>2%</span></div>
                </li>
            </ul>
        </div>

        <div class="bookmark__item bookmark__item--exchange">

            <div class="bookmark__row bookmark__row--header">
                <div class="bookmark__col align-left">
                    <span>From</span>
                </div>
                <div class="bookmark__col align-right">
                    <span>To</span>
                </div>
            </div>

            <ul class="bookmark__col align-left">
                <li class="bookmark__row bookmark__row--body ">
                    <span class="exchange-from exchange-from--active" data-from="wm">Webmoney</span>
                </li>
                <li class="bookmark__row bookmark__row--body ">
                    <span class="exchange-from " data-from="pm">Perfectmoney</span>
                </li>
                <li class="bookmark__row bookmark__row--body ">
                    <span class="exchange-from " data-from="qiwi">Qiwi</span>
                </li>
                <li class="bookmark__row bookmark__row--body ">
                    <span class="exchange-from " data-from="sb">Sberbank</span>
                </li>
            </ul>

            <ul class="bookmark__col align-right">
                <li class="bookmark__row--body wm">
                    <div class="bookmark__row">
                        <a href="#" class="underlined exchange-to">Qiwi</a>
                    </div>
                    <div class="bookmark__row">
                        <a href="#" class="underlined exchange-to ">Paypal</a>
                    </div>
                </li>
                <li class="bookmark__row--body bookmark__row--body--hidden sb">
                    <div class="bookmark__row">
                        <a href="#" class="underlined exchange-to">Qiwi</a>
                    </div>
                    <div class="bookmark__row">
                        <a href="#" class="underlined exchange-to ">Webmoney</a>
                    </div>
                    <div class="bookmark__row">
                        <a href="#" class="underlined exchange-to ">Western Union</a>
                    </div>
                    <div class="bookmark__row">
                        <a href="#" class="underlined exchange-to ">Qiwi</a>
                    </div>
                </li>
                <li class="bookmark__row--body bookmark__row--body--hidden qiwi">
                    <div class="bookmark__row">
                        <a href="#" class="underlined exchange-to ">Webmoney</a>
                    </div>
                    <div class="bookmark__row">
                        <a href="#" class="underlined exchange-to ">Western Union</a>
                    </div>
                    <div class="bookmark__row">
                        <a href="#" class="underlined exchange-to ">Qiwi</a>
                    </div>
                </li>
                <li class="bookmark__row--body bookmark__row--body--hidden pm">
                    <div class="bookmark__row">
                        <a href="#" class="underlined exchange-to ">Webmoney</a>
                    </div>
                    <div class="bookmark__row">
                        <a href="#" class="underlined exchange-to ">Western Union</a>
                    </div>
                    <div class="bookmark__row">
                        <a href="#" class="underlined exchange-to ">Qiwi</a>
                    </div>
                    <div class="bookmark__row">
                        <a href="#" class="underlined exchange-to ">Perfect Money</a>
                    </div>
                    <div class="bookmark__row">
                        <a href="#" class="underlined exchange-to ">Paypal</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <form class="terms__bookmark">
        <div class="title4 title4--red terms__title">Get free bitcoin!</div>
        <div class="terms__text">
            <p>Registered users can receive free daily Bitcoin of 0.10 – 0.50 USD (not more than once in 24 hours).For last 24 hours we paind 432 bonuses. Perfect Money </p>
        </div>
        <div class="error error--bookmark">
            <p>Извините, на данный момент заявки не выполняются. Зайдите позже.</p>
        </div>
        <input class="form__input form__input--whitefield form__input--terms" placeholder="Type your bitcoin address"></input>
        <button class="button button--turquoise" type="submit">Get it!</button>
    </form>
</div>