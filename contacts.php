<div class="background" style="height: 570px;"></div>
<div class="container-1230px">
    <div class="container-820px">
        <div class="contacts" style="min-height: 910px;">
            <h1>Contacts</h1>
            <div class="text-greenfield">
                <p>Dear customers! Please be aware that all our contacts are listed below. If someone contacts you from other
                    account and says he is Cryptex24 director or manager please know that he is trying to cheat you!Also please
                    remember we never exchange in messengers or social networks!
                </p>
            </div>
            <div class="text-greenfield">
                <div class="twocol">
                    <div class="title2">General questions</div>
                    <table>
                        <tbody>
                            <tr>
                                <td class="contacts__title">Email:</td>
                                <td class="contacts__text">support@cryptex24.com</td>
                            </tr>
                            <tr>
                                <td class="contacts__title">Skype:</td>
                                <td class="contacts__text">cryptex24com</td>
                            </tr>
                            <tr>
                                <td class="contacts__title">Icq:</td>
                                <td class="contacts__text">13123123</td>
                            </tr>
                            <tr>
                                <td class="contacts__title">Jabber:</td>
                                <td class="contacts__text">jaber@cryptex24.com</td>
                            </tr>
                        </tbody>
                    </table>
                </div><!--
             --><div class="twocol">
                    <div class="title2">Affiliate program</div>
                    <table>
                        <tbody>
                            <tr>
                                <td class="contacts__title">Email:</td>
                                <td class="contacts__text">support@cryptex24.com</td>
                            </tr>
                            <tr>
                                <td class="contacts__title">Skype:</td>
                                <td class="contacts__text">cryptex24com</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <form class="whitefield padded-field">
                <div class="title4">Обратная связь</div>
                <div class="text-whitefield">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua, consectetur adipisicing elit.
                    </p>
                </div>
                <div class="error">
                    <p>Извините, на данный момент заявки не выполняются. Зайдите позже.</p>
                </div>
                <div class="form__row">
                    <label>
                        <div class="form__title">Your email:</div>
                        <input type="text" class="form__input form__input--whitefield form__input--250">
                    </label>
                     <label>
                        <div class="form__title">Questions:</div>
                        <textarea type="text" class="form__input form__textarea form__input--whitefield form__input--510"></textarea>
                    </label>
                </div>
                <button type="submit" class="button button--red float-right">
                    <span class="button__text">Send message</span>
                    <span class="button__image"></span>
                </button>
                <img class="loader--big float-right" src="static/desktop/images/loader.gif">
            </form>
        </div>
    </div>
</div>
