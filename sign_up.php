<div class="background" style="height: 570px;"></div>
<div class="container-1230px">
    <div class="container-820px">
        <div class="sign_up" style="min-height: 910px;">
            <h1>Sign up</h1>
            <form class="padded-field autorization-form">
                <p class="text-greenfield">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore
                magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea.</p>
                <div class="error">
                    <p>Извините, на данный момент заявки не выполняются. Зайдите позже.</p>
                </div>
                <div class="whitefield">
                    <div class="title4">Account information</div>
                    <div class="form__row">
                        <label>
                            <div class="form__title">Your email:</div>
                            <input type="text" class="form__input form__input--whitefield form__input--350">
                        </label>
                        <label>
                            <div class="form__title">Account password:</div>
                            <input type="text" class="form__input form__input--whitefield form__input--350">
                            <a href="#" class="underlined">Show password</a>
                        </label>
                    </div>
                </div>
                <p class="text-greenfield">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore
                magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea.</p>
                <div class="whitefield">
                    <div class="title4">Personal information:</div>
                    <div class="twocol">
                        <label>
                            <div class="form__title">First name:</div>
                            <input type="text" class="form__input form__input--whitefield">
                        </label>
                        <label class="form--error">
                            <div class="form__title">Last name:</div>
                            <input type="text" class="form__input form__input--whitefield">
                        </label>
                        <label>
                            <div class="form__title">Middle name:</div>
                            <input type="text" class="form__input form__input--whitefield">
                        </label>
                    </div><!--
                --><div class="twocol">
                        <label class="form--error">
                            <div class="form__title">City:</div>
                            <input type="text" class="form__input form__input--whitefield">
                        </label>
                        <label>
                            <div class="form__title">Country:</div>
                            <input type="text" class="form__input form__input--whitefield">
                        </label>
                    </div>
                </div>
                <button type="submit" class="button button--red float-right">
                    <span class="button__text">Sign up</span>
                    <span class="button__image"></span>
                </button>
                 <img class="loader--big float-right" src="static/desktop/images/loader.gif">
            </form>
        </div>
    </div>
</div>
