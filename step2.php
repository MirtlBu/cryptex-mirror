<div class="background" style="height: 570px;"></div>
<div class="container-1230px">
    <div class="container-820px">
        <div class="step2" style="min-height: 620px;">
            <h1>Step 2</h1>
            <p class="text-greenfield">You are not registered user. Please enter your password for automatic registration.</p>
            <form class="padded-field autorization-form">
                <div class="error">
                    <p>Извините, на данный момент заявки не выполняются. Зайдите позже.</p>
                </div>
                <div class="whitefield">
                    <div class="title4">Account information</div>
                    <div class="form__row">
                        <label>
                            <div class="form__title">Account password:</div>
                            <input type="text" class="form__input form__input--whitefield form__input--350">
                            <a href="#" class="underlined">Show password</a>
                        </label>
                    </div>

                </div>
                <div class="whitefield">
                    <div class="title4">Exchange information:</div>
                    <div class="text-whitefield">Еще одна инструкция: реквизиты указаны ниже, пожалуйста, проверьте  правильность введенных данных
                        перед переводом.
                    </div>
                    <div class="form__row">
                        <label class="form--error">
                            <div class="form__title">Target account:</div>
                            <input type="text" class="form__input form__input--whitefield form__input--350">
                        </label>
                    </div>
                </div>
                <div class="whitefield">
                    <div class="title4">Personal information:</div>
                    <div class="twocol">
                        <label>
                            <div class="form__title">First name:</div>
                            <input type="text" class="form__input form__input--whitefield">
                        </label>
                        <label class="form--error">
                            <div class="form__title">Last name:</div>
                            <input type="text" class="form__input form__input--whitefield">
                        </label>
                        <label>
                            <div class="form__title">Middle name:</div>
                            <input type="text" class="form__input form__input--whitefield">
                        </label>
                    </div><!--
                --><div class="twocol">
                        <label class="form--error">
                            <div class="form__title">City:</div>
                            <input type="text" class="form__input form__input--whitefield">
                        </label>
                        <label>
                            <div class="form__title">Country:</div>
                            <input type="text" class="form__input form__input--whitefield">
                        </label>
                    </div>
                </div>
                <button type="submit" class="button button--red float-right">
                    <span class="button__text">Save order</span>
                    <span class="button__image"></span>
                </button>
                 <img class="loader--big float-right" src="static/desktop/images/loader.gif">
            </form>
        </div>
    </div>
</div>
