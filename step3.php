<div class="background" style="height: 570px;"></div>
<div class="container-1230px">
    <div class="container-820px">
        <div class="step3" style="min-height: 620px;">
            <h1>Step 3</h1>
            <p class="text-greenfield">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore
magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex eacommodo consequat.
            </p>
            <div class="whitefield from-to">
                <ul>
                    <li class="tcell tcell--50px">
                        <div class="currency-img"></div>
                    </li>
                    <li class="tcell tcell--200px">
                        <div class="title4">100 bitcoin btc</div>
                    </li>
                    <li class="tcell tcell--100px">
                        <div class="from-to-2"></div>
                    </li>
                    <li class="tcell tcell--50px">
                        <div class="currency-img"></div>
                    </li>
                    <li class="tcell tcell--250px">
                        <div class="title4">10 000 westernunion usd</div>
                    </li>
                </ul>
            </div>
            <div class="greenfield">
                <div class="twocol">
                    <div class="timer">
                        <div class="timer__img"></div>
                        <div class="timer__text">1 : 59 : 03</div>
                    </div>

                </div><!--
            --><div class="twocol">
                    <button type="submit" class="button button--red float-right">
                        <span class="button__text">Оплатить заказ</span>
                        <span class="button__image"></span>
                    </button>
                </div>
            </div>
            <div class="whitefield padded-field">
                <div class="title4">Information</div>
                <div class="text-whitefield">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua, consectetur adipisicing elit.
                    </p>
                </div>
                <div class="order-invoice">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td class="order__title text-whitefield align-right">Банк:</td>
                                <td class="order__text align-left">Альфа Банк</td>
                            </tr>
                            <tr>
                                <td class="order__title text-whitefield align-right">Получатель платежа:</td>
                                <td class="order__text align-left">Иванов Иван Иванович</td>
                            </tr>
                            <tr>
                                <td class="order__title text-whitefield align-right">Номер счета получателя:</td>
                                <td class="order__text align-left">25900109590235</td>
                            </tr>
                            <tr>
                                <td class="order__title text-whitefield align-right">Примечания к платежу:</td>
                                <td class="order__text align-left">частный перевод, ндс нет</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="text-whitefield">
                    <p>
                    Еще одна инструкция : реквизиты указаны ниже, пожалуйста, проверьте  правильность введенных данных перед переводом.
                    </p>
                </div>
                <button type="submit" class="button button--red float-right">
                    <span class="button__text">Подтвердить оплату</span>
                    <span class="button__image"></span>
                </button>
                <img class="loader--big float-right" src="static/desktop/images/loader.gif">
            </div>
        </div>
    </div>
</div>
